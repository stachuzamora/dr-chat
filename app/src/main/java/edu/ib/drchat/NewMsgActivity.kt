package edu.ib.drchat

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import edu.ib.drchat.models.User
import kotlinx.android.synthetic.main.activity_new_msg.*
import kotlinx.android.synthetic.main.new_msg_item.view.*

class NewMsgActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_msg)

        supportActionBar?.title = "Wybierz pacjenta"


        val adapter = GroupAdapter<GroupieViewHolder>()


        recycler_view_newmsg.adapter = adapter

        fetchUsers()
    }

    companion object {
        val DOCTOR_KEY = "DOCTOR_KEY"
    }

    private fun fetchUsers() {
        val ref = FirebaseDatabase.getInstance()
            .getReference("/users")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {

                val adapter = GroupAdapter<GroupieViewHolder>()

                snapshot.children.forEach {

                    val user = it.getValue(User::class.java)
//                    Log.d("NewMsg", user.toString())
                    if (user != null) {
                        adapter.add(UserItem(user))
                    }
                }

                adapter.setOnItemClickListener { item, view ->

                    val userItem = item as UserItem

                    run {

                        val intent = Intent(view.context, ChatLogActivity::class.java)
//                        Log.d("INTENT", username)
                        intent.putExtra(DOCTOR_KEY, userItem.user)
                        startActivity(intent)

                        finish()
                    }
                }

                recycler_view_newmsg.adapter = adapter
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }

}

class UserItem(val user: User) : Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.username_newmsg.text = user.username

//        val storageRef = FirebaseStorage.getInstance().getReference("/avatars/")
//        val rand = (0..5).random()
//        val pathReference = storageRef.child(user.sex + rand)
//        val url = pathReference.downloadUrl
//
//        Picasso.get().load(url.toString()).into(viewHolder.itemView.user_avatar)
//        Log.d("Found image reference:", pathReference.toString())
    }

    override fun getLayout(): Int {
        return R.layout.new_msg_item
    }
}

