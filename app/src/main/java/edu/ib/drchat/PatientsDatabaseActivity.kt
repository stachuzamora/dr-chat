package edu.ib.drchat

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import edu.ib.drchat.models.User
import kotlinx.android.synthetic.main.activity_patients_database.*
import kotlinx.android.synthetic.main.patient_item.view.*
import kotlinx.android.synthetic.main.toolbar_patients.*

class PatientsDatabaseActivity : AppCompatActivity() {

    companion object {
        val DOCTOR = "DOCTOR"
    }

    val doctorsPatients = HashMap<String, User>()
    private val adapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patients_database)

        patients_database_recycler_view.adapter = adapter
        patients_database_recycler_view.addItemDecoration(
            DividerItemDecoration(
                patients_database_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )

        if (sort_patients_edit_text != null) {
            sort_patients_edit_text.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    Log.d("PATIENT", "S: $s")
                    if (s.isNullOrEmpty()) {
                        refreshRecyclerView()
                    } else {
                        adapter.clear()
                        val newUsersMap = HashMap<String, User>()
                        doctorsPatients.forEach {
                            val user = it.value
                            if (s in user.username.toLowerCase()) {
                                adapter.add(PatientItem(user))
                            }
                        }
                    }
                }

                override fun afterTextChanged(s: Editable?) {

                }

            })

        }


        getDoctorsPatients()

        if (toolbar_patients_title != null) {
            toolbar_patients_title.text = "Twoi pacjenci"
        }

        if(back_button_patients != null) {
            back_button_patients.setOnClickListener {
                finish()
            }
        }
    }

    private fun refreshRecyclerView() {

        adapter.clear()
        if (doctorsPatients.isNotEmpty()) {
            val sortedMap = doctorsPatients.toList()
                .sortedBy { (_, value) -> value.username.split(' ')[1] }
                .sortedBy { (_, value) -> value.username.split(' ')[0] }.toMap()
            sortedMap.forEach {
                adapter.add(PatientItem(it.value))
            }

        } else {
            adapter.add(EmptyPatientsItem())
        }

        Log.d("PATIENT", "ADAPTER SIZE: ${adapter.groupCount}")

    }

    private fun getDoctorsPatients() {

        val uid = Firebase.auth.uid

        val refPatients = FirebaseDatabase.getInstance()
            .getReference("/doctor_patients/$uid/")

        refPatients.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                val patient = snapshot.getValue(User::class.java)
                Log.d("PATIENT", "VALUE: ${snapshot.value}")

                Log.d("PATIENT", "UID: ${patient?.uid}")
                if (patient != null) {
                    doctorsPatients[patient.uid] = patient
                    refreshRecyclerView()
                }

                if (doctorsPatients.isNotEmpty()) {
                    doctorsPatients.forEach {
                        Log.d("PATIENT", "MAP AFTER CHILD ADDED ${it.key}")
                    }
                }

            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val patient = snapshot.getValue(User::class.java)

                if (patient != null) {
                    doctorsPatients[patient.uid] = patient
                    refreshRecyclerView()
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

    }

    class PatientItem(private val patient: User) : Item<GroupieViewHolder>() {
        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.patient_database_name.text = patient.username
            viewHolder.itemView.setOnClickListener {

                val params = viewHolder.itemView.layoutParams
                val heightOne = viewHolder.itemView.patient_database_name.height
                val heightTwo = viewHolder.itemView.chat_patient_button.height

                Log.d("PATIENT", "HEIGHT ONE: $heightOne, HEIGHT TWO: $heightTwo")

                if (viewHolder.itemView.patient_options_view.visibility == View.INVISIBLE) {

                    params.height = heightOne + heightTwo + 50

                    viewHolder.itemView.patient_options_view.animate().withStartAction {
                        viewHolder.itemView.layoutParams = params
                        viewHolder.itemView.patient_options_view.visibility = View.VISIBLE

                    }.translationY((heightTwo).toFloat()).start()
                } else {
                    params.height = heightOne + 50
                    viewHolder.itemView.patient_options_view.animate().translationY(0f)
                        .withStartAction {
                            viewHolder.itemView.patient_options_view.visibility = View.INVISIBLE
                            viewHolder.itemView.layoutParams = params
                        }.start()

                }
            }

            viewHolder.itemView.chat_patient_button.setOnClickListener {
                val intent = Intent(it.context, DoctorChatLogActivity::class.java)
                intent.putExtra(DoctorChatLogActivity.USER, patient)
                it.context.startActivity(intent)
            }

            viewHolder.itemView.prescription_patient_button.setOnClickListener {
                val intent = Intent(it.context, PrescriptionActivity::class.java)
                intent.putExtra(PrescriptionActivity.PATIENT, patient)
                it.context.startActivity(intent)
            }
        }

        override fun getLayout(): Int {
            return R.layout.patient_item
        }

    }

    class EmptyPatientsItem : Item<GroupieViewHolder>() {
        override fun bind(viewHolder: GroupieViewHolder, position: Int) {

        }

        override fun getLayout(): Int {
            return R.layout.request_row_item_empty
        }

    }
}
