package edu.ib.drchat

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import edu.ib.drchat.ChatLogActivity.Companion.DOCTOR_KEY
import edu.ib.drchat.ChatLogActivity.Companion.USER_KEY
import edu.ib.drchat.MenuActivity.Companion.currentUser
import edu.ib.drchat.models.Doctor
import edu.ib.drchat.models.PrescriptionItem
import edu.ib.drchat.models.Request
import edu.ib.drchat.models.User
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.fragment_prescription.*
import kotlinx.android.synthetic.main.prescription_item.view.*
import kotlinx.android.synthetic.main.request_row_item_chat.view.*
import kotlinx.android.synthetic.main.toolbar.*
import java.sql.Date
import java.text.SimpleDateFormat

class MenuActivity : AppCompatActivity() {

    val adapterRequests = GroupAdapter<GroupieViewHolder>()
    val adapterPrescriptions = GroupAdapter<GroupieViewHolder>()
    var currentPosition = 0
    val prescriptionsMap = HashMap<String, Prescription>()

    companion object {
        var currentUser: User? = null
        var leadDoctor: Doctor? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val ref = FirebaseDatabase.getInstance()
            .getReference("/users/")


        var roleIsUser = false
        val uid = Firebase.auth.uid

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {
                    if (uid == it.getValue(User::class.java)?.uid) {
                        roleIsUser = true
                    }
                }

                if (roleIsUser) {
                    setContentView(R.layout.activity_menu)

                    current_requests_recycler_view.adapter = adapterRequests


                    setupPrescriptions()

                    current_requests_recycler_view.addItemDecoration(
                        DividerItemDecoration(
                            current_requests_recycler_view.context,
                            DividerItemDecoration.VERTICAL
                        )
                    )

                    refreshCurrentRequestsRecycleView()
                    verifyUserLoggedIn()
                    fetchCurrentUser()

                    listenForRequests()
//                    listenForMessageNotification()

//        lead_doctor.text = ""

                    chat_request.setOnClickListener {
                        makeChatRequest()
//            val newFragment = ConfirmRequestDialogFragment(leadDoctorUsername!!)
//
//            supportFragmentManager.beginTransaction()
//
//            val dialogAnswer = newFragment.show(supportFragmentManager, "confirm")
//
//            Log.d("REQ", "Dialog window result: $dialogAnswer")

//            val intent = Intent(this, NewMsgActivity::class.java)
//            startActivity(intent)
                    }


                    prescription_request.setOnClickListener {
                        makePrescriptionRequest()
                    }


                    if (logout_button_patient != null) {

                        logout_button_patient
                            .setOnClickListener {
                                FirebaseAuth.getInstance().signOut()
                                val intent = Intent(it.context, MainActivity::class.java)
                                intent.flags =
                                    Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intent)
                            }

                    }


                } else {

                    val intent = Intent(this@MenuActivity, DoctorMenuActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)

//                    setContentView(R.layout.activity_doctor_menu)

                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })


    }

    private fun refreshPrescriptions() {

        val listOfFinished = arrayListOf<PrescriptionItem>()
        val listOfNotFinished = arrayListOf<PrescriptionItem>()
        val listOfAll = arrayListOf<PrescriptionItem>()

        val sortedMap = prescriptionsMap.toList().sortedBy { (_, value) -> value.timestamp }.toMap()

        sortedMap.forEach {
            Log.d("PRESC", "MAP KEY: ${it.key}, VALUE: ${it.value}")
            val prescriptionDate = timestampToDate(it.value.timestamp)
            val expirationDate =
                timestampToDate(it.value.timestamp + (it.value.expiration * 3600 * 24))
            Log.d("PRES", "")
            val code = it.value.code

            val prescriptionItem =
                PrescriptionItem(
                    prescriptionDate!!,
                    expirationDate!!,
                    code.toString(),
                    it.value.status,
                    it.key,
                    it.value
                )

            listOfAll.add(prescriptionItem)

            if (it.value.status) {
                listOfFinished.add(
                    prescriptionItem
                )
            } else {
                listOfNotFinished.add(
                    prescriptionItem
                )
            }
            scrollToBottom()
        }


        val firstFragment = PrescriptionFragment(listOfNotFinished)
        val secondFragment = PrescriptionFragment(listOfFinished)
        val thirdFragment = PrescriptionFragment(listOfAll)

        val listOfFragments = listOf(firstFragment, secondFragment, thirdFragment)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.prescription_fragment_placeholder, listOfFragments[currentPosition])
            commit()
        }
        scrollToBottom()
    }

    private fun timestampToDate(timestamp: Long): String? {
        return try {
            val sdf = SimpleDateFormat("dd.MM.yyyy")
            val netDate = Date(timestamp * 1000)
            sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }

    private fun setupPrescriptions() {

        val listOfCurrentPrescViewLabels = listOf("Niezrealizowane", "Zrealizowane", "Wszystkie")

        val uid = Firebase.auth.uid

        val refPrescriptions = FirebaseDatabase.getInstance()
            .getReference("/prescriptions/$uid/")

        refPrescriptions.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                Log.d("PRESC", "SNAPSHOT VALUE: ${snapshot.value}")
                Log.d("PRESC", "SNAPSHOT KEY: ${snapshot.key}")

                val prescription = snapshot.getValue(Prescription::class.java)
//
                if (prescription != null) {
                    prescriptionsMap[snapshot.key.toString()] = prescription
                }
                refreshPrescriptions()
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("PRESC", "CHANGED SNPASHOT VALUE: ${snapshot.value}")
                Log.d("PRESC", "CHANGED SNPASHOT KEY: ${snapshot.key}")

                val prescription = snapshot.getValue(Prescription::class.java)
                if (prescription != null) {
                    prescriptionsMap[snapshot.key.toString()] = prescription
                }
                refreshPrescriptions()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                prescriptionsMap.remove(snapshot.key)
                refreshPrescriptions()
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })


//        val listOfItems = listOf(
//            PrescriptionItem("25.11.2020", "25.12.2020", "4299", true),
//            PrescriptionItem("04.12.2020", "18.12.2020", "9329", false),
//            PrescriptionItem("25.11.2020", "25.12.2020", "4829", true)
//        )
//
//        val listOfItems2 = listOf(
//            PrescriptionItem("25.11.2020", "25.12.2020", "4299", true),
//            PrescriptionItem("04.12.2020", "18.12.2020", "9329", false),
//            PrescriptionItem("25.11.2020", "25.12.2020", "4829", true),
//            PrescriptionItem("25.11.2020", "25.12.2020", "4299", true),
//            PrescriptionItem("04.12.2020", "18.12.2020", "9329", false),
//            PrescriptionItem("25.11.2020", "25.12.2020", "4829", true)
//        )
//
//        val listOfItems3 = listOf(
//            PrescriptionItem("25.11.2020", "25.12.2020", "4829", true)
//        )

        if (left_arrow != null) {
            left_arrow.setOnClickListener {

                currentPosition = when (currentPosition) {
                    0 -> 2
                    1 -> 0
                    2 -> 1
                    else -> 0
                }
                refreshPrescriptions()

                current_presc_view.text = listOfCurrentPrescViewLabels[currentPosition]

            }
        }

        if (right_arrow != null) {

            right_arrow.setOnClickListener {
                currentPosition = when (currentPosition) {
                    0 -> 1
                    1 -> 2
                    2 -> 0
                    else -> 0
                }

                refreshPrescriptions()

                current_presc_view.text = listOfCurrentPrescViewLabels[currentPosition]

            }
        }

    }

    private fun scrollToBottom() {
        menu_scroll_view.post {
            run {
                menu_scroll_view.scrollTo(0, menu_scroll_view.bottom)
            }
        }
    }

//    private fun checkForExistingRequests(): Boolean {
//
//        val uid = Firebase.auth.uid
//        val refChats =
//            FirebaseDatabase.getInstance().getReference("/requests/chats/${leadDoctor?.uid}/$uid")
//        val requestsList = arrayListOf<Request>()
//
//        refChats.addListenerForSingleValueEvent(object : ValueEventListener {
//
//            override fun onDataChange(snapshot: DataSnapshot) {
//
//                snapshot.children.forEach {
//
//                    val currentTimestamp = System.currentTimeMillis() / 1000
//
//
//                    val req = it.getValue(Request::class.java)
//
//                    if (req != null) {
//                        if (!req.completed) {
//                            requestsList.add(req)
//                        }
//
//                    }
//                }
//
//                when {
//                    requestsList.size == 0 -> {
//                        adapter_requests.add(EmptyRequestItem())
//                    }
//                    requestsList.size > 1 -> {
//                        Log.d("REQ", "More than 1")
//                    }
//                    else -> {
//                        val req = requestsList[0]
//                        Log.d("REQ", "Exactly one")
//
//                        val type = if (req.type == 0) {
//                            "Czat"
//                        } else {
//                            "Recepta"
//                        }
//
//                        adapter_requests.add(RequestItem(type, leadDoctor!!))
//                    }
//                }
//
//                current_requests_recycler_view.adapter = adapter_requests
//
//            }
//
//            override fun onCancelled(error: DatabaseError) {
//                TODO("Not yet implemented")
//            }
//        })
//        return false
//
//    }

    private val latestRequests = HashMap<String, Request>()

    private fun refreshCurrentRequestsRecycleView() {
        adapterRequests.clear()
        if (latestRequests.isNotEmpty()) {
            latestRequests.forEach {
                if (it.value.type == 0) {
                    adapterRequests.add(RequestChatItem("Czat", leadDoctor!!))
                } else {
                    adapterRequests.add(RequestPrescriptionItem("Recepta", leadDoctor!!))
                }
            }
        } else {
            adapterRequests.add(EmptyRequestItem())
        }
    }

    //
    private fun listenForRequests() {
        val uid = Firebase.auth.uid


        val chatRef = FirebaseDatabase.getInstance()
            .getReference("/requests/chats/$uid/")

        chatRef.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val req = snapshot.getValue(Request::class.java)
                val key = snapshot.key + "0"
                Log.d("REQ", "SNAPSHOT ADDED CHAT KEY:$key")
                latestRequests[key] = req!!
                chat_request.isEnabled = false
                refreshCurrentRequestsRecycleView()
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val req = snapshot.getValue(Request::class.java)
                val key = snapshot.key + "0"
                latestRequests[key] = req!!
                chat_request.isEnabled = false
                refreshCurrentRequestsRecycleView()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                chat_request.isEnabled = true
                val key = snapshot.key + "0"
                latestRequests.remove(key)
                refreshCurrentRequestsRecycleView()
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

        val prescRef = FirebaseDatabase.getInstance()
            .getReference("/requests/prescriptions/$uid")

        prescRef.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val req = snapshot.getValue(Request::class.java)
                val key = snapshot.key + "1"
                Log.d("REQ", "SNAPSHOT ADDED PRESCRIPTION KEY:$key")
                latestRequests[key] = req!!
                prescription_request.isEnabled = false
                refreshCurrentRequestsRecycleView()
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val req = snapshot.getValue(Request::class.java)
                val key = snapshot.key + "1"
                latestRequests[key] = req!!
                prescription_request.isEnabled = false
                refreshCurrentRequestsRecycleView()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                prescription_request.isEnabled = true
                val key = snapshot.key + "1"
                latestRequests.remove(key)
                refreshCurrentRequestsRecycleView()
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

    }

    private fun makePrescriptionRequest() {

        if (currentUser == null) {
            Log.w("REQ", "Empty current user")
        } else {

            val refUser = FirebaseDatabase.getInstance()
                .getReference("/requests/prescriptions/${currentUser?.lead_doctor}/${currentUser?.uid}")

            val request =
                Request(
                    refUser.key!!,
                    1,
                    currentUser!!.lead_doctor,
                    System.currentTimeMillis() / 1000
                )

            refUser.setValue(request)
                .addOnSuccessListener {
                    Toast.makeText(
                        this,
                        "Prośba o receptę została dodana",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                .addOnFailureListener {
                    Toast.makeText(
                        this,
                        "Dodanie prośby nie powiodło się - spróbuj ponownie",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            val refDoctor = FirebaseDatabase.getInstance()
                .getReference("/requests/prescriptions/${currentUser?.uid}/${currentUser?.lead_doctor}")

            refDoctor.setValue(request)
        }
    }

    private fun makeChatRequest() {

        if (currentUser == null) {
            Log.w("REQ", "Empty current user")
        } else {

            Log.d(
                "REQ",
                "leadDoctorId=${currentUser!!.lead_doctor} and currentUserId=${currentUser!!.uid}"
            )

            val ref = FirebaseDatabase.getInstance()
                .getReference("/requests/chats/${currentUser!!.lead_doctor}/${currentUser!!.uid}")
            val request =
                Request(
                    ref.key!!,
                    0,
                    currentUser!!.lead_doctor,
                    System.currentTimeMillis() / 1000
                )
            ref.setValue(request)
                .addOnSuccessListener {
                    Toast.makeText(
                        this,
                        "Prośba o rozmowę online została dodana",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                .addOnFailureListener {
                    Toast.makeText(
                        this,
                        "Dodanie prośby nie powiodło się - spróbuj ponownie",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            val refReqToMe = FirebaseDatabase.getInstance()
                .getReference("/requests/chats/${currentUser!!.uid}/${currentUser!!.lead_doctor}")

            refReqToMe.setValue(request)
        }
    }


    private fun fetchCurrentUser() {
        val refUsers = FirebaseDatabase.getInstance().getReference("/users")

        var leadDoctorId: String? = null

        refUsers.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {


                snapshot.children.forEach {
                    val user = it.getValue(User::class.java)

                    if (user != null) {
                        if (user.uid == FirebaseAuth.getInstance().uid) {
                            currentUser = user
                            logged_in_user.text = "Witaj, ${user.username}"
                            leadDoctorId = user.lead_doctor
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })

        val refDoctors = FirebaseDatabase.getInstance().getReference("/doctors/")

        refDoctors.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {
                snapshot.children.forEach {

                    val doctor = it.getValue(Doctor::class.java)

                    if (doctor != null) {
                        if (doctor.uid == leadDoctorId) {
                            leadDoctor = doctor
                            lead_doctor.text = leadDoctor!!.username
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }


    private fun verifyUserLoggedIn() {

        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//
//        when (item.itemId) {
////            R.id.menu_new_msg -> {
////                val intent = Intent(this, NewMsgActivity::class.java)
////                startActivity(intent)
////            }
//            R.id.menu_sign_out -> {
//                FirebaseAuth.getInstance().signOut()
//                val intent = Intent(this, MainActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
//                startActivity(intent)
//            }
//        }
//
//        val itemId = item.itemId
//
//        return super.onOptionsItemSelected(item)
//    }
//
//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.nav_menu, menu)
//        return super.onCreateOptionsMenu(menu)
//    }

}

//class ConfirmRequestDialogFragment(val doctorName: String) : DialogFragment() {
//
//    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return activity?.let {
//            val builder = AlertDialog.Builder(it)
//            builder.setMessage("Czy na pewno chcesz prosić o rozmowę z dr $doctorName?")
//                .setPositiveButton("Tak", DialogInterface.OnClickListener { dialog, which ->
//                    run {
//                        sendRequest()
//                    }
//                })
//                .setNegativeButton("Nie", DialogInterface.OnClickListener { dialog, which ->
//                    run {
//                        dialog.dismiss()
//                    }
//                })
//            builder.create()
//        } ?: throw IllegalStateException("Activity cannot be null")
//    }
//
//    private fun sendRequest() {
//        Log.d("REQ", "Request sent")
//    }
//
//}

class RequestPrescriptionItem(private val type: String, private val doctor: Doctor) :
    Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.request_type.text = type
        viewHolder.itemView.doctor_name.text = doctor.username
        viewHolder.itemView.cancel_button.setOnClickListener {

            val uid = Firebase.auth.uid

            val refUser = FirebaseDatabase.getInstance()
                .getReference("/requests/prescriptions/$uid")
            refUser.removeValue()

            val refDoctor = FirebaseDatabase.getInstance()
                .getReference("/requests/prescriptions/${doctor.uid}/$uid")
            refDoctor.removeValue()
        }

    }

    override fun getLayout(): Int {
        return R.layout.request_row_item_perscription
    }
}

class RequestChatItem(
    private val type: String, private val doctor: Doctor
) : Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.request_type.text = type
        viewHolder.itemView.doctor_name.text = doctor.username
        viewHolder.itemView.open_button.setOnClickListener {
            Log.d("REQ", "Position: $position")

            val intent = Intent(it.context, ChatLogActivity::class.java)
            intent.putExtra(DOCTOR_KEY, doctor)
            intent.putExtra(USER_KEY, currentUser)
            it.context.startActivity(intent)
        }
        viewHolder.itemView.cancel_button.setOnClickListener {

            val uid = Firebase.auth.uid
            val doctorUid = doctor.uid

            val refUser = FirebaseDatabase.getInstance()
                .getReference("/requests/chats/$uid")
            refUser.removeValue()

            val refDoctor = FirebaseDatabase.getInstance()
                .getReference("/requests/chats/${doctorUid}/$uid")
            refDoctor.removeValue()
        }
    }

    override fun getLayout(): Int {
        return R.layout.request_row_item_chat
    }


    private fun calcApproxWaitTime() {
        TODO("Not yet implemented")
    }
}

class EmptyRequestItem : Item<GroupieViewHolder>() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
    }

    override fun getLayout(): Int {
        return R.layout.request_row_item_empty
    }

}

