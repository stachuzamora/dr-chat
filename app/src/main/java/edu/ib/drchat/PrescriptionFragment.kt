package edu.ib.drchat

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import edu.ib.drchat.models.PrescriptionItem
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.fragment_prescription.*

class PrescriptionFragment(private val prescriptionItems: List<PrescriptionItem>) : Fragment(R.layout.fragment_prescription) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val adapter = GroupAdapter<GroupieViewHolder>()

        prescriptionItems.forEach {
            adapter.add(it)
        }

        prescriptions_finished_recycler_view.adapter = adapter

        prescriptions_finished_recycler_view.addItemDecoration(
            DividerItemDecoration(
                prescriptions_finished_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )
    }
}