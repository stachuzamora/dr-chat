package edu.ib.drchat

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import edu.ib.drchat.models.User
import kotlinx.android.synthetic.main.activity_prescription.*
import kotlinx.android.synthetic.main.toolbar_prescription.*

class PrescriptionActivity : AppCompatActivity() {

    companion object {
        val PATIENT = "PATIENT"
    }

    var patient: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prescription)

        patient = intent.getParcelableExtra<User>(PATIENT)

        if (for_text_view != null && patient != null) {
            for_text_view.text = patient?.username
        }

//        val arrayAdapter = new Arra
        button_send_prescription.setOnClickListener {
            sendPrescription()
            val intent = Intent(it.context, DoctorMenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.and(Intent.FLAG_ACTIVITY_NEW_TASK)
            it.context.startActivity(intent)
        }


        if (back_button_prescription != null) {
            back_button_prescription.setOnClickListener {
                finish()
            }
        }

    }


    private fun sendPrescription() {

        val uid = Firebase.auth.uid

        if (patient != null) {
            val refPrescription = FirebaseDatabase.getInstance()
                .getReference("/prescriptions/${patient?.uid}/").push()

            val timestamp = System.currentTimeMillis() / 1000

            val expirationTime = when (expiration_spinner.selectedItemPosition) {
                0 -> 7
                1 -> 30
                2 -> 120
                else -> 0
            }

            val code = code_edit_text.text.toString()

            if (code.isEmpty()) {
                Toast.makeText(
                    this,
                    "Pole z kodem recepty nie może zostać puste",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else {

                if (code.length < 4) {
                    Toast.makeText(
                        this,
                        "Kod musi być 4-cyfrowy",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } else {
                    val prescription = Prescription(timestamp, expirationTime, code.toLong(), false)
                    refPrescription.setValue(prescription)

                    val refRequestPatient = FirebaseDatabase.getInstance()
                        .getReference("/requests/prescriptions/${patient?.uid}/$uid")
                    refRequestPatient.removeValue()

                    val refRequestDoctor = FirebaseDatabase.getInstance()
                        .getReference("/requests/prescriptions/$uid/${patient?.uid}")
                    refRequestDoctor.removeValue()
                }
            }
        }
    }
}