package edu.ib.drchat

import android.content.Intent
import android.opengl.Visibility
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        supportActionBar?.setIcon(R.drawable.logo)
//        supportActionBar?.setDisplayUseLogoEnabled(true)

        password_forgot_label.setOnClickListener {
            val intent = Intent(this, NewPasswordActivity::class.java)
            startActivity(intent)
        }

        email_edit_text.setOnFocusChangeListener { v, hasFocus ->
            run {
                if (wrong_password_prompt.isVisible and hasFocus)
                    wrong_password_prompt.visibility = View.INVISIBLE
            }
        }

        password_edit_text.setOnFocusChangeListener { v, hasFocus ->
            run {
                if (wrong_password_prompt.isVisible and hasFocus)
                    wrong_password_prompt.visibility = View.INVISIBLE
            }
        }
        login_button.setOnClickListener {
            performLogIn()
        }
    }

    private fun performLogIn() {

        auth = Firebase.auth
        val email = email_edit_text.text.toString()
        val password = password_edit_text.text.toString()

        if (email.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Uzupełnij wszystkie pola, aby się zalogować", Toast.LENGTH_SHORT)
                .show()
            return
        }

        auth.signInWithEmailAndPassword(email, password).addOnSuccessListener {

            val intent = Intent(this, MenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)

        }.addOnFailureListener {
            run { wrong_password_prompt.visibility = View.VISIBLE }
//            Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
            return@addOnFailureListener

        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}


