package edu.ib.drchat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_new_password.*
import kotlin.concurrent.thread

class NewPasswordActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_password)


        reset_password_button.
                setOnClickListener {

                    val email = new_pass_email_edit_text.text.toString()

                    if (email.isEmpty()) {
                        Toast.makeText(this,"Pole z adresem email musi być uzupełnione",
                            Toast.LENGTH_SHORT).show()
                    } else {
                        val auth = FirebaseAuth.getInstance().sendPasswordResetEmail(email)
                            .addOnCompleteListener {
                                    Toast.makeText(this, "Jeśli podany został poprawny mail, link do resetowania hasła został przesłany", Toast.LENGTH_LONG)
                                        .show()
                                    val intent = Intent(this, MainActivity::class.java)
                                    startActivity(intent)

                            }.addOnFailureListener { return@addOnFailureListener }
                    }

                }

    }
}