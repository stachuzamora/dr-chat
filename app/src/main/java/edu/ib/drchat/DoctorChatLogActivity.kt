package edu.ib.drchat

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isGone
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import edu.ib.drchat.models.ChatMessage
import edu.ib.drchat.models.Doctor
import edu.ib.drchat.models.User
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.activity_doctor_chat_log.*
import kotlinx.android.synthetic.main.activity_doctor_chat_log.send_button
import kotlinx.android.synthetic.main.activity_doctor_chat_log.view.*
import kotlinx.android.synthetic.main.chat_row_from.view.*
import kotlinx.android.synthetic.main.chat_row_to.view.*
import kotlinx.android.synthetic.main.ready_message_item.view.*
import kotlinx.android.synthetic.main.toolbar_doctor_chat.*
import java.sql.Time

class DoctorChatLogActivity : AppCompatActivity() {

    companion object {
        val USER = "USERNAME"
        var patientUid: String? = null
        var patient: User? = null
        var currentUser: Doctor? = null
    }

    val adapter = GroupAdapter<GroupieViewHolder>()

    val readyMsgAdapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_chat_log)

        val uid = Firebase.auth.uid

        val ref = FirebaseDatabase.getInstance()
            .getReference("/doctors/$uid")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                val doctor = snapshot.getValue(Doctor::class.java)
                if (doctor != null) {
                    currentUser = doctor
                }
            }

            override fun onCancelled(error: DatabaseError) {
            }

        })

        patient = intent.getParcelableExtra(USER)
        patientUid = patient?.uid

//        if (patient != null) {
//            supportActionBar?.title = patient?.username
//            patientUid = patient?.uid
//        } else {
//            supportActionBar?.title = "Czat"
//        }

        chat_field_recycler_view_doctor.adapter = adapter

        ready_msg_recycler_view.adapter = readyMsgAdapter

        ready_msg_recycler_view.addItemDecoration(
            DividerItemDecoration(
                ready_msg_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )


        close_ready_msg_btn.setOnClickListener {
            close_ready_msg_btn.visibility = View.GONE
            ready_msg_recycler_view.visibility = View.GONE
        }



        listenForMessages()

        makeMeAvailable()
        checkForPatientStatus()
        setupAdapter()
        messageRead()

        /**
         * Adding setOnClickListener to send button that will invoke performSendMessage function,
         * which sends message
         */
        send_button.setOnClickListener {
            performSendMessage()
        }

        if (toolbar_doctor_chat_title != null) {
            toolbar_doctor_chat_title.text = patient?.username
        }

        if (back_doctor_chat_button != null) {
            back_doctor_chat_button.setOnClickListener {
                finish()
            }
        }

        if (ready_msg_button != null) {
            ready_msg_button.setOnClickListener {
                if (ready_msg_recycler_view.isGone) {
                    ready_msg_recycler_view.visibility = View.VISIBLE
                    close_ready_msg_btn.visibility = View.VISIBLE
                    setupAdapter()
                    chat_field_recycler_view_doctor.scrollToPosition(adapter.itemCount - 1)


                } else {
                    close_ready_msg_btn.visibility = View.GONE
                    ready_msg_recycler_view.visibility = View.GONE
                }
            }
        }

        if (msg_doctor_edit_text != null) {
            msg_doctor_edit_text.setOnClickListener {
                if (chat_field_recycler_view_doctor != null) {
                    chat_field_recycler_view_doctor.scrollToPosition(adapter.itemCount - 1)
                }
                close_ready_msg_btn.visibility = View.GONE
                ready_msg_recycler_view.visibility = View.GONE
            }
            msg_doctor_edit_text.setOnFocusChangeListener { v, hasFocus ->
                run {
                    if (hasFocus) {
                        if (chat_field_recycler_view_doctor != null) {
                            chat_field_recycler_view_doctor.scrollToPosition(adapter.itemCount - 1)
                        }
                        close_ready_msg_btn.visibility = View.GONE
                        ready_msg_recycler_view.visibility = View.GONE
                    }
                }
            }
        }

    }

    private fun messageRead() {

//        val uid = Firebase.auth.uid
//
//        val refNewMsgPatient = FirebaseDatabase.getInstance()
//            .getReference("/new_messages/$patientUid/$uid")
//
//        refNewMsgPatient.setValue(NewMessageObject(false))

    }


    private fun setupAdapter() {

        var to = ""
        var dear = ""
        var whom = ""
        if (patient?.sex == "male") {
            to = "Pana"
            dear = "Panie"
            whom = "Panu"
        } else {
            to = "Pani"
            dear = to
            whom = to
        }

        readyMsgAdapter.clear()

        readyMsgAdapter.add(ReadyMessageItem("Witam!"))
        readyMsgAdapter.add(
            ReadyMessageItem(
                "Witam, $dear ${
                    patient?.username?.split(" ")?.get(1)
                }."
            )
        )
        readyMsgAdapter.add(ReadyMessageItem("Co mogę dla $to zrobić?"))
        readyMsgAdapter.add(ReadyMessageItem("Dzień dobry. W czym mogę $whom pomóc?"))
        readyMsgAdapter.add(ReadyMessageItem("Witam! Jak mogę $whom pomóc?"))
        readyMsgAdapter.add(ReadyMessageItem("Dzień dobry. Jak zdrowie?"))
        readyMsgAdapter.add(ReadyMessageItem("Dzień dobry. Co tam słychać?"))

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.ready_msg_button -> {
                if (ready_msg_recycler_view.isGone) {
                    ready_msg_recycler_view.visibility = View.VISIBLE
                    close_ready_msg_btn.visibility = View.VISIBLE
                    setupAdapter()
                    chat_field_recycler_view_doctor.scrollToPosition(adapter.itemCount - 1)


                } else {
                    close_ready_msg_btn.visibility = View.GONE
                    ready_msg_recycler_view.visibility = View.GONE
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun makeMeAvailable() {

        val uid = Firebase.auth.uid
        val patientUid = patient?.uid

        val refDatabase = FirebaseDatabase.getInstance()
            .getReference("/in_chat/${uid}/$patientUid")

        refDatabase.setValue(InChatAvailability(true))
    }

    private fun makeMeUnavailable() {
        val uid = Firebase.auth.uid
        val patientUid = patient?.uid

        val refDatabase = FirebaseDatabase.getInstance()
            .getReference("/in_chat/${uid}/$patientUid")

        refDatabase.setValue(InChatAvailability(false))
    }

    private fun checkForPatientStatus() {

        val uid = patient?.uid

        val refDatabase = FirebaseDatabase.getInstance()
            .getReference("/in_chat/$uid")

        refDatabase.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val isPatientInChat = snapshot.getValue(Boolean::class.java)

                if (isPatientInChat != null) {

                    if (isPatientInChat) {
                        patient_availability.text = "Dostępny"
                        patient_status_logo.setImageResource(R.drawable.ic_baseline_account_green)
                    } else {
                        patient_availability.text = "Niedostępny"
                        patient_status_logo.setImageResource(R.drawable.ic_baseline_account_gray)
                    }

                }
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val isPatientInChat = snapshot.getValue(Boolean::class.java)

                if (isPatientInChat != null) {

                    if (isPatientInChat) {
                        patient_availability.text = "Dostępny"
                        patient_status_logo.setImageResource(R.drawable.ic_baseline_account_green)
                    } else {
                        patient_availability.text = "Niedostępny"
                        patient_status_logo.setImageResource(R.drawable.ic_baseline_account_gray)
                    }

                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

    }

    override fun onDestroy() {
        makeMeUnavailable()
        super.onDestroy()
    }

    private fun performSendMessage() {
        val msg = msg_doctor_edit_text.text.toString()

        if (msg.isNotEmpty()) {

//        Log.d("MSG", msg)

            val idFrom = FirebaseAuth.getInstance().uid

            val idTo = patient?.uid

            if (idFrom == null) return

//        val ref = FirebaseDatabase.getInstance().getReference("/messages").push()
            val ref =
                FirebaseDatabase.getInstance().getReference("/user-messages/$idFrom/$idTo").push()

            val refTo =
                FirebaseDatabase.getInstance().getReference("/user-messages/$idTo/$idFrom").push()

//        Log.d("MSG", ref.key.toString())
            val chatMessage = ChatMessage(
                ref.key!!, msg, idTo!!, idFrom,
                System.currentTimeMillis() / 1000
            )

            ref.setValue(chatMessage)
                .addOnSuccessListener {
//                Log.d("MSG", "Saved our chat message: ${ref.key}")
                    msg_doctor_edit_text.text.clear()
                }

            refTo.setValue(chatMessage)
                .addOnSuccessListener {
//                Log.d("MSG", "Saved \"recieved chat message ")
                }

            val refNewMsgPatient = FirebaseDatabase.getInstance()
                .getReference("/new_messages/$idTo/$idFrom/")

            refNewMsgPatient.setValue(NewMessageObject(true))
        } else {
            Toast.makeText(this, "Nie można wysłać pustej wiadomości", Toast.LENGTH_SHORT)
                .show()
        }

    }

    private fun listenForMessages() {
        val idFrom = FirebaseAuth.getInstance().uid
        val idTo = patient?.uid

        val ref = FirebaseDatabase.getInstance().getReference("/user-messages/$idFrom/$idTo")

        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val chatMessage = snapshot.getValue(ChatMessage::class.java)
//                Log.d("MSG", "onChildAdded msg: ${chatMessage?.text}")

                if (chatMessage != null) {
                    if (chatMessage.idFrom == FirebaseAuth.getInstance().uid) {
                        adapter.add(
                            ChatItemFrom(
                                chatMessage.text,
                                chatMessage.timestamp
                            )
                        )
                    } else {
                        adapter.add(
                            ChatItemTo(
                                chatMessage.text,
                                chatMessage.timestamp
                            )
                        )
                    }
                    chat_field_recycler_view_doctor.scrollToPosition(adapter.itemCount - 1)

                }

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }
        }
        )

    }

    class ChatItemFrom(private val text: String, private val time: Long) :
        Item<GroupieViewHolder>() {

        private fun getTimeFromTimestamp(timestamp: Long): String? {
            val time = Time(timestamp).toString()
            return time.subSequence(0, 5).toString()
        }

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.msg_text_view_from.text = text
            viewHolder.itemView.text_clock_from.text = getTimeFromTimestamp(time * 1000)

            val username = currentUser?.username?.split(' ')

            val initials = username?.get(1)?.substring(0, 1) + username?.get(2)?.substring(0, 1)

            viewHolder.itemView.avatar_view_from.text = initials.toUpperCase()
        }

        override fun getLayout(): Int {
            return R.layout.chat_row_from
        }
    }

    class ChatItemTo(private val text: String, private val time: Long) : Item<GroupieViewHolder>() {

        private fun getTimeFromTimestamp(timestamp: Long): String? {
            val time = Time(timestamp).toString()
            return time.subSequence(0, 5).toString()
        }

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.msg_text_view_to.text = text
            viewHolder.itemView.text_clock_to.text = getTimeFromTimestamp(time * 1000)

            val patientName = patient?.username?.split(' ')

            val initials =
                patientName?.get(0)?.substring(0, 1) + patientName?.get(1)?.substring(0, 1)

            viewHolder.itemView.avatar_view_to.text = initials.toUpperCase()


        }

        override fun getLayout(): Int {
            return R.layout.chat_row_to
        }
    }


    class ReadyMessageItem(private val text: String) : Item<GroupieViewHolder>() {
        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.ready_msg_item_text.text = text

            viewHolder.itemView.setOnClickListener {

                val idFrom = FirebaseAuth.getInstance().uid

                val idTo = patientUid

                if (idFrom == null) return@setOnClickListener

//        val ref = FirebaseDatabase.getInstance().getReference("/messages").push()
                val ref =
                    FirebaseDatabase.getInstance().getReference("/user-messages/$idFrom/$idTo")
                        .push()

                val refTo =
                    FirebaseDatabase.getInstance().getReference("/user-messages/$idTo/$idFrom")
                        .push()

//        Log.d("MSG", ref.key.toString())
                val chatMessage = ChatMessage(
                    ref.key!!, text, idTo!!, idFrom,
                    System.currentTimeMillis() / 1000
                )

                ref.setValue(chatMessage)
                refTo.setValue(chatMessage)

                val refNewMsgPatient = FirebaseDatabase.getInstance()
                    .getReference("/new_messages/$idTo/$idFrom/")

                refNewMsgPatient.setValue(NewMessageObject(true))


            }

        }

        override fun getLayout(): Int {
            return R.layout.ready_message_item
        }
    }
}

class InChatAvailability(val in_chat: Boolean) {
    constructor() : this(false)
}