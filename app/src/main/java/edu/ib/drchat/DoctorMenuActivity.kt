package edu.ib.drchat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.api.fallback.service.FirebaseAuthFallbackService
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import edu.ib.drchat.models.Doctor
import edu.ib.drchat.models.Request
import edu.ib.drchat.models.User
import kotlinx.android.synthetic.main.activity_doctor_menu.*
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.request_chat_row_item_doctor.view.*
import kotlinx.android.synthetic.main.request_prescription_item_doctor.view.*
import kotlinx.android.synthetic.main.toolbar_doctor.*

class DoctorMenuActivity : AppCompatActivity() {

    val doctorsPatients = HashMap<String, User>()
    val chatsHashMap = HashMap<String, Request>()
    val prescriptionHashMap = HashMap<String, Request>()
    val chatAdapter = GroupAdapter<GroupieViewHolder>()
    val prescriptionAdapter = GroupAdapter<GroupieViewHolder>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doctor_menu)

        current_chat_requests_recycler_view.adapter = chatAdapter
        current_chat_requests_recycler_view.addItemDecoration(
            DividerItemDecoration(
                current_chat_requests_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )

        current_prescription_requests_recycler_view.adapter = prescriptionAdapter
        current_prescription_requests_recycler_view.addItemDecoration(
            DividerItemDecoration(
                current_chat_requests_recycler_view.context,
                DividerItemDecoration.VERTICAL
            )
        )

        getDoctorsPatients()
        verifyUserLoggedIn()
        fetchCurrentUser()
        listenForChatRequests()
        listenForPrescriptionRequests()
        refreshPrescriptionRecyclerView()
        refreshChatRecycleView()

        if (patients_database_button != null) {
            patients_database_button.setOnClickListener {
                val intent = Intent(this, PatientsDatabaseActivity::class.java)
                startActivity(intent)
            }
        }

        if (logout_button_doctor != null) {

            logout_button_doctor
                .setOnClickListener {
                    FirebaseAuth.getInstance().signOut()
                    val intent = Intent(it.context, MainActivity::class.java)
                    intent.flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
        }
    }

    private fun getDoctorsPatients() {

        val uid = Firebase.auth.uid
        val refPatients = FirebaseDatabase.getInstance()
            .getReference("/users/")

        refPatients.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val patient = snapshot.getValue(User::class.java)
                if (patient != null) {
                    if (patient.lead_doctor == uid) {
                        doctorsPatients[patient.uid] = patient
                    }
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })

    }

    private fun listenForChatRequests() {

        val uid = Firebase.auth.uid

        val refChats = FirebaseDatabase.getInstance()
            .getReference("/requests/chats/$uid")

        refChats.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                Log.d("DOC", "SNAPSHOT ON CHILD ADDED KEY: ${snapshot.key}")

                val request = snapshot.getValue(Request::class.java)

                if (request != null) {
                    chatsHashMap[snapshot.key.toString()] = request
                }

                refreshChatRecycleView()
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val request = snapshot.getValue(Request::class.java)

                if (request != null) {
                    chatsHashMap[snapshot.key.toString()] = request
                }

                refreshChatRecycleView()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

                chatsHashMap.remove(snapshot.key)

                refreshChatRecycleView()

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }

    private fun listenForPrescriptionRequests() {

        val uid = Firebase.auth.uid

        val refPrescription = FirebaseDatabase.getInstance()
            .getReference("/requests/prescriptions/$uid/")

        refPrescription.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                val request = snapshot.getValue(Request::class.java)

                Log.d("PRESC_REQ", "ID: ${request?.id}")

                if (request != null) {
                    prescriptionHashMap[snapshot.key.toString()] = request
                }

                refreshPrescriptionRecyclerView()
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val request = snapshot.getValue(Request::class.java)

                Log.d("PRESC_REQ", "ID on changed: ${request?.id}")
                if (request != null) {
                    prescriptionHashMap[snapshot.key.toString()] = request
                }

                refreshPrescriptionRecyclerView()
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                val key = snapshot.key
                prescriptionHashMap.remove(key)
                refreshPrescriptionRecyclerView()
                Log.d("PRESC_REQ", "ID on remove: ${snapshot.key}")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })
    }

    private fun refreshPrescriptionRecyclerView() {

        prescriptionAdapter.clear()
        if (prescriptionHashMap.isNotEmpty()) {
            val sortedMap = prescriptionHashMap.toList()
                .sortedBy { (_, value) -> value.timestamp }
                .sortedBy { (_, value) -> value.timestamp }
                .toMap()
            sortedMap.forEach {
                val request = it.value
                if (doctorsPatients.isNotEmpty()) {
                    val patient = doctorsPatients[request.id]
                    prescriptionAdapter.add(RequestPrescriptionItemDoctor(request, patient!!))
                }
            }
        } else {
            prescriptionAdapter.add(EmptyRequestPrescriptionItemDoctor())
        }
    }

    private fun refreshChatRecycleView() {

        chatAdapter.clear()
        if (chatsHashMap.isNotEmpty()) {
            val sortedMap = chatsHashMap.toList()
                .sortedBy { (_, value) -> value.timestamp }
                .sortedBy { (_, value) -> value.timestamp }
                .toMap()
            sortedMap.forEach {
                val request = it.value
                if (doctorsPatients.isNotEmpty()) {
                    val patient = doctorsPatients[request.id]
                    chatAdapter.add(RequestChatItemDoctor(request, patient!!))
                }
            }
        } else {
            chatAdapter.add(EmptyRequestChatItemDoctor())
        }
    }

    private fun fetchCurrentUser() {

        val uid = Firebase.auth.uid

        val refDoctors = FirebaseDatabase.getInstance().getReference("/doctors/")

        refDoctors.addListenerForSingleValueEvent(object : ValueEventListener {

            override fun onDataChange(snapshot: DataSnapshot) {


                snapshot.children.forEach {
                    Log.d("DOC", "SNAPSHOT UID: ${it.key}")
                    val doctor = it.getValue(Doctor::class.java)

                    if (doctor != null) {
                        if (doctor.uid == uid) {
                            logged_in_doctor.text = "Witaj, ${doctor.username}"
                        }
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }
        })
    }


    private fun verifyUserLoggedIn() {

        val uid = FirebaseAuth.getInstance().uid
        if (uid == null) {
            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK.or(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

}

class RequestChatItemDoctor(private val request: Request, private val patient: User) :
    Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {

        val uid = Firebase.auth.uid
        val patientUid = patient.uid


        viewHolder.itemView.patient_name.text = patient.username
        viewHolder.itemView.wait_time.text = request.getHourOfRequest()
        viewHolder.itemView.open_chat_button.setOnClickListener {

            val intent = Intent(it.context, DoctorChatLogActivity::class.java)
            intent.putExtra(DoctorChatLogActivity.USER, patient)
            it.context.startActivity(intent)

        }



        viewHolder.itemView.end_chat_button.setOnClickListener {


            val refDoc = FirebaseDatabase.getInstance()
                .getReference("/requests/chats/$uid/$patientUid")

            refDoc.removeValue().addOnSuccessListener {
                Toast.makeText(
                    viewHolder.itemView.context,
                    "Konsultacja zakończona",
                    Toast.LENGTH_SHORT
                ).show()
            }

            val refPatient = FirebaseDatabase.getInstance()
                .getReference("/requests/chats/$patientUid/$uid")

            refPatient.removeValue()
        }

    }

    override fun getLayout(): Int {
        return R.layout.request_chat_row_item_doctor
    }

}

class EmptyRequestChatItemDoctor() : Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
    }

    override fun getLayout(): Int {
        return R.layout.request_chat_row_item_empty_doctor
    }

}

class RequestPrescriptionItemDoctor(private val request: Request, private val patient: User) :
    Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.patient_presc_name.text = patient.username
        viewHolder.itemView.wait_presc_time.text = request.getHourOfRequest()

        viewHolder.itemView.open_prescription_button.setOnClickListener {
            val intent = Intent(it.context, PrescriptionActivity::class.java)
            intent.putExtra(PrescriptionActivity.PATIENT, patient)
            it.context.startActivity(intent)
        }

    }

    override fun getLayout(): Int {
        return R.layout.request_prescription_item_doctor
    }

}

class EmptyRequestPrescriptionItemDoctor() : Item<GroupieViewHolder>() {
    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
    }

    override fun getLayout(): Int {
        return R.layout.request_prescription_item_empty_doctor
    }

}

class NewMessageObject(val newMsg: Boolean) {
    constructor() : this(true)
}
