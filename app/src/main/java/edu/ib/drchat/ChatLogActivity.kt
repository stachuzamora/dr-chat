package edu.ib.drchat

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import edu.ib.drchat.models.ChatMessage
import edu.ib.drchat.models.Doctor
import edu.ib.drchat.models.User
import kotlinx.android.synthetic.main.activity_chat_log.*
import kotlinx.android.synthetic.main.chat_row_from.view.*
import kotlinx.android.synthetic.main.chat_row_to.view.*
import kotlinx.android.synthetic.main.toolbar_chat_patient.*
import java.sql.Time

class ChatLogActivity : AppCompatActivity() {

    companion object {
        val DOCTOR_KEY = "DOCTOR"
        val USER_KEY = "KEY"
        var currentUser: User? = null
        var doctor: Doctor? = null
    }

    val adapter = GroupAdapter<GroupieViewHolder>()
//    var isDoctorAvailable = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_log)

        chat_field_recycler_view.adapter = adapter

        doctor = intent.getParcelableExtra(DOCTOR_KEY)

        currentUser = intent.getParcelableExtra(USER_KEY)

//        if (doctor != null) {
//            supportActionBar?.title = doctor?.username
//        } else {
//            supportActionBar?.title = "Czat"
//        }


        msg_edit_text.isEnabled = false
        send_button.isEnabled = false

        checkForChatStartInfo()
        listenForMessages()

        messageRead()

        makeMeAvailable()
        /**
         * Adding setOnClickListener to send button that will invoke performSendMessage function,
         * which sends message
         */
        send_button.setOnClickListener {
            performSendMessage()
        }

        if (toolbar_patient_chat_title != null) {
            toolbar_patient_chat_title.text = doctor?.username
        }

        if (back_button_chat_patient != null) {
            back_button_chat_patient.setOnClickListener {
                finish()
            }
        }
        if (msg_edit_text != null) {
            msg_edit_text.setOnClickListener {
                if (chat_field_recycler_view != null) {
                    chat_field_recycler_view.scrollToPosition(adapter.itemCount - 1)
                }
            }
            msg_edit_text.setOnFocusChangeListener { v, hasFocus ->
                run {
                    if (hasFocus) {
                        if (chat_field_recycler_view != null) {
                            chat_field_recycler_view.scrollToPosition(adapter.itemCount - 1)
                        }
                    }
                }
            }
        }
    }


    private fun messageRead() {
        val uid = Firebase.auth.uid
        val doctorUid = doctor?.uid

        val refNewMsg = FirebaseDatabase.getInstance()
            .getReference("/new_messages/$uid/$doctorUid")

        refNewMsg.setValue(NewMessageObject(false))
    }


//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when(item.itemId) {
//            R.id.doctor_availability -> {
//
//                item.setIcon(R.drawable.ic_baseline_account_green)
//            }
//        }
//
//        return super.onOptionsItemSelected(item)
//    }

    override fun onDestroy() {
        makeMeUnavailable()
        super.onDestroy()
    }

    private fun makeMeAvailable() {

        val uid = Firebase.auth.uid
        val refDatabase = FirebaseDatabase.getInstance()
            .getReference("/in_chat/$uid")

        refDatabase.setValue(InChatAvailability(true))
    }

    private fun makeMeUnavailable() {
        val uid = Firebase.auth.uid

        val refDatabase = FirebaseDatabase.getInstance()
            .getReference("/in_chat/$uid")

        refDatabase.setValue(InChatAvailability(false))
    }

    private fun checkForChatStartInfo() {

        val uid = Firebase.auth.uid
        val doctorUid = doctor?.uid

        val refInChat = FirebaseDatabase.getInstance()
            .getReference("/in_chat/$doctorUid/$uid/")

        refInChat.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val isDoctorInChat = snapshot.getValue(Boolean::class.java)
                Log.d(
                    "CHAT",
                    "SNAPSHOT FROM CHATLOG VALUE: ${snapshot.value} KEY: ${snapshot.key} "
                )
                Log.d("CHAT", "Is doctor in chat: $isDoctorInChat")

                if (isDoctorInChat!!) {
                    send_button.isEnabled = true
                    msg_edit_text.isEnabled = true
//                    is_doctor_in_chat_text_view.visibility = View.INVISIBLE
                    doctor_availability.text = "Dostępny"
                    status_logo.setImageResource(R.drawable.ic_baseline_account_green)
                } else {

                    send_button.isEnabled = false
                    msg_edit_text.isEnabled = false
//                    is_doctor_in_chat_text_view.visibility = View.VISIBLE
                    doctor_availability.text = "Niedostępny"
                    status_logo.setImageResource(R.drawable.ic_baseline_account_gray)
                    Toast.makeText(
                        applicationContext,
                        "Aktualnie nie możesz wysyłać wiadomości. Opcja ta" +
                                " będzie dostępna, gdy tylko lekarz pojawi się na czacie",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                val isDoctorInChat = snapshot.getValue(Boolean::class.java)
                Log.d(
                    "CHAT",
                    "SNAPSHOT FROM CHATLOG VALUE: ${snapshot.value} KEY: ${snapshot.key} "
                )
                Log.d("CHAT", "Is doctor in chat: $isDoctorInChat")

                if (isDoctorInChat!!) {
                    send_button.isEnabled = true
                    msg_edit_text.isEnabled = true
//                    is_doctor_in_chat_text_view.visibility = View.INVISIBLE
                    doctor_availability.text = "Dostępny"
                    status_logo.setImageResource(R.drawable.ic_baseline_account_green)
//                    supportActionBar?.setIcon(R.drawable.ic_baseline_account_green)

                } else {
                    send_button.isEnabled = false
                    msg_edit_text.isEnabled = false
//                    is_doctor_in_chat_text_view.visibility = View.VISIBLE
                    doctor_availability.text = "Niedostępny"
                    status_logo.setImageResource(R.drawable.ic_baseline_account_gray)
                    Toast.makeText(
                        applicationContext,
                        "Aktualnie nie możesz wysyłać wiadomości. Opcja ta" +
                                " będzie dostępna, gdy tylko lekarz pojawi się na czacie",
                        Toast.LENGTH_LONG
                    )
                        .show()
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

        })


    }

    private fun listenForMessages() {

        val idFrom = FirebaseAuth.getInstance().uid
        val idTo = doctor?.uid

        val ref = FirebaseDatabase.getInstance().getReference("/user-messages/$idFrom/$idTo")

        ref.addChildEventListener(object : ChildEventListener {
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                val chatMessage = snapshot.getValue(ChatMessage::class.java)
//                Log.d("MSG", "onChildAdded msg: ${chatMessage?.text}")

                if (chatMessage != null) {
                    if (chatMessage.idFrom == FirebaseAuth.getInstance().uid) {
                        adapter.add(ChatItemFrom(chatMessage.text, chatMessage.timestamp))
                    } else {
                        adapter.add(ChatItemTo(chatMessage.text, chatMessage.timestamp))
                    }
                    chat_field_recycler_view.scrollToPosition(adapter.itemCount - 1)

                }

            }

            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {
                TODO("Not yet implemented")
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                TODO("Not yet implemented")
            }
        }
        )

    }


    private fun performSendMessage() {


        val msg = msg_edit_text.text.toString()

        if (msg.isNotEmpty()) {

//        Log.d("MSG", msg)

            val idFrom = FirebaseAuth.getInstance().uid

            val idTo = doctor?.uid

            if (idFrom == null) return

//        val ref = FirebaseDatabase.getInstance().getReference("/messages").push()
            val ref =
                FirebaseDatabase.getInstance().getReference("/user-messages/$idFrom/$idTo").push()

            val refTo =
                FirebaseDatabase.getInstance().getReference("/user-messages/$idTo/$idFrom").push()

//        Log.d("MSG", ref.key.toString())
            val chatMessage = ChatMessage(
                ref.key!!, msg, idTo!!, idFrom,
                System.currentTimeMillis() / 1000
            )

            ref.setValue(chatMessage)
                .addOnSuccessListener {
//                Log.d("MSG", "Saved our chat message: ${ref.key}")
                    msg_edit_text.text.clear()
                }

            refTo.setValue(chatMessage)
                .addOnSuccessListener {
//                Log.d("MSG", "Saved \"recieved chat message ")
                }
        } else {
            Toast.makeText(this, "Nie można wysłać pustej wiadomości", Toast.LENGTH_SHORT)
                .show()
        }

    }

    class ChatItemFrom(private val text: String, private val time: Long) :
        Item<GroupieViewHolder>() {

        private fun getTimeFromTimestamp(timestamp: Long): String? {
            val time = Time(timestamp).toString()
            return time.subSequence(0, 5).toString()
        }

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.msg_text_view_from.text = text
            viewHolder.itemView.text_clock_from.text = getTimeFromTimestamp(time * 1000)

            val username = currentUser?.username?.split(' ')

            val initials = username?.get(0)?.substring(0, 1) + username?.get(1)?.substring(0, 1)

            viewHolder.itemView.avatar_view_from.text = initials.toUpperCase()
        }

        override fun getLayout(): Int {
            return R.layout.chat_row_from
        }
    }

    class ChatItemTo(private val text: String, private val time: Long) : Item<GroupieViewHolder>() {

        private fun getTimeFromTimestamp(timestamp: Long): String? {
            val time = Time(timestamp).toString()
            return time.subSequence(0, 5).toString()
        }

        override fun bind(viewHolder: GroupieViewHolder, position: Int) {
            viewHolder.itemView.msg_text_view_to.text = text
            viewHolder.itemView.text_clock_to.text = getTimeFromTimestamp(time * 1000)

            val doctorName = doctor?.username?.split(' ')

            val initials = doctorName?.get(1)?.substring(0, 1) + doctorName?.get(2)?.substring(0, 1)

            viewHolder.itemView.avatar_view_to.text = initials.toUpperCase()
        }

        override fun getLayout(): Int {
            return R.layout.chat_row_to
        }
    }
}

