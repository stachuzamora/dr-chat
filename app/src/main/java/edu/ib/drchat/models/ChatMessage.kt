package edu.ib.drchat.models


class ChatMessage(val id: String, val text: String, val idTo : String,
                  val idFrom: String, val timestamp: Long) {
    constructor() : this("", "", "", "", -1)
}