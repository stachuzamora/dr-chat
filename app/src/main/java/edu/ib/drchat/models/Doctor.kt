package edu.ib.drchat.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Doctor(
    val uid: String, val username: String, val sex: String, val email: String, val role: String) : Parcelable {
    constructor() : this("", "", "", "", "")
}