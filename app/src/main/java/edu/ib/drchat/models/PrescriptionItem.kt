package edu.ib.drchat.models

import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item
import edu.ib.drchat.Prescription
import edu.ib.drchat.R
import kotlinx.android.synthetic.main.prescription_item.view.*

class PrescriptionItem(
    private val prescribe_date: String,
    private val exp_date: String,
    private val code: String,
    private val done: Boolean,
    private val key: String,
    private val prescription: Prescription
) : Item<GroupieViewHolder>() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        viewHolder.itemView.prescribe_date.text = prescribe_date
        viewHolder.itemView.expire_date.text = exp_date
        viewHolder.itemView.prescription_code.text = code

        val uid = Firebase.auth.uid

        viewHolder.itemView.prescription_status.setOnClickListener {

            if (!done) {

                val ref = FirebaseDatabase.getInstance()
                    .getReference("/prescriptions/$uid/$key")

                prescription.status = true

                ref.setValue(prescription).addOnSuccessListener {
                    viewHolder.itemView.prescription_status.setImageResource(R.drawable.ic_baseline_check_24)
                }

            }

        }

        if (done) {
            viewHolder.itemView.prescription_status.setImageResource(R.drawable.ic_baseline_check_24)
        } else {
            viewHolder.itemView.prescription_status.setImageResource(R.drawable.ic_baseline_check_box_outline_blank_24)
        }
    }

    override fun getLayout(): Int {
        return R.layout.prescription_item
    }

}
