package edu.ib.drchat.models

class Request(val id: String, val type: Int, val doctorId: String, val timestamp: Long) {
    constructor(): this("",-1, "", -1)


    fun getHourOfRequest() : String{

        val sdf = java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val date = java.util.Date(timestamp * 1000)
        val time = sdf.format(date)
        return time.subSequence(11,16).toString()
    }
}