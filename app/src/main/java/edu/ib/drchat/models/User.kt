package edu.ib.drchat.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(
    val uid: String, val username: String, val sex: String, val email: String, val role: String,
        val lead_doctor: String) : Parcelable {
    constructor() : this("", "", "", "", "", "")
}